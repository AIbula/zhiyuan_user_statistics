<?php

namespace Zhiyuan\Statistics;

class BaseClient
{

    protected $redisHandle = null;
    protected static $container = [];

    private function __construct($handle)
    {
        if (!is_null($handle)) {
            $this->redisHandle = $handle;
        }
        return $this->redisHandle;
    }

    /**
     * 服务对象实例
     * @return static
     */
    public static function getInstance()
    {
        return static::$container[static::class] ?? NULL;
    }


    /**
     * 服务注册
     */
    public static function register($provider = NULL)
    {
        if ($provider instanceof \Redis) {
            if (array_key_exists(static::class, static::$container) == false || empty(static::$container[static::class])) {
                static::$container[static::class] = new static($provider);
            }
        } else {
            throw new \Exception('provider is not Redis instance', -1);
        }
    }

}