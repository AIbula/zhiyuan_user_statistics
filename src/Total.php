<?php

namespace Zhiyuan\Statistics;

class Total extends BaseClient
{

    /**
     * @param $params ['type'] 场景类型：1用户总数
     * @param $params ['value']
     * @return array
     */
    public function saveUser(array $params = [])
    {
        try {
            $result = [
                'code' => 1,
                'msg' => '',
                'data' => []
            ];
            if (empty($params['value'])) {
                $result['msg'] = '参数错误';
                return $result;
            }

            $lua = <<<SCRIPT
                local data = KEYS[1]
                local data = cjson.decode(data)

                for type, v in pairs(data) do
                    redis.call("HINCRBY","Statistics:Total:User",type,v)
                end
                return true
            SCRIPT;

            $typeArr = self::getTypeArr('saveUser', $params['value']);
            if (empty($typeArr)) {
                throw new \Exception('更新失败', 1);
            }
            $status = $this->redisHandle->eval($lua, [json_encode($typeArr)], 1);
            if (!$status) {
                throw new \Exception('更新失败', 2);
            }

            $result['code'] = 0;
            $result['msg'] = '更新成功';
        } catch (\Exception $e) {
            $result['code'] = $e->getCode();
            $result['msg'] = $e->getMessage();
        }

        return $result;
    }

    public static function getTypeArr($action, $value)
    {
        $value = intval($value);
        $result = [];
        switch ($action) {
            case 'saveUser':
                $typeArr = [
                    'adminUserList',
                    'userAccount',
                ];
                foreach ($typeArr as $type) {
                    $result[$type] = $value;
                }
        }
        return $result;
    }

    /**
     * @param $type 场景类型：1用户总数
     * @return array
     */
    public function getUser(array $typeArr)
    {
        try {
            $result = [
                'code' => 1,
                'msg' => '',
                'data' => []
            ];

            foreach ($typeArr as $type) {
                $data[$type] = $this->redisHandle->HGET('Statistics:Total:User', $type);
            }

            $result['code'] = 0;
            $result['msg'] = '获取成功';
            $result['data'] = $data;
        } catch (\Exception $e) {
            $result['msg'] = $e->getMessage();
        }

        return $result;
    }
}
